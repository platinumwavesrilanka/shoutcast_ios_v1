//
//  PlayViewController+RFPlayViewController.m
//  ShoutCast
//
//  Created by Platinum Lanka Pvt Ltd on 3/31/17.
//  Copyright © 2017 Platinum Lanka Pvt Ltd. All rights reserved.
//

#import "PlayViewController+RFPlayViewController.h"

@implementation PlayViewController (RFPlayViewController)

-(void)viewDidLoad {
    [super viewDidLoad];
    
    [self.artist setTextColor:[UIColor blackColor]];
}

@end
