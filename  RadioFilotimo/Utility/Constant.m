//
//  Constant.m
//  SoundCast
//
//  Created by Platinum Lanka Pvt Ltd on 1/2/17.
//  Copyright © 2017 Platinum Lanka Pvt Ltd. All rights reserved.
//

#import "Constant.h"

@implementation Constant

// Title
NSString *const kTitle = @"Radio - Filotimo";

// Streaming constants
NSString *const kStreamIpAddress = @"http://109.236.85.141:7114/stream";

// Calling and SMS
NSString *const kPhoneNumber = @"00447448675750";
NSString *const kSMSBody = @"";

// Email
NSString *const kEmailTitle = @"Title";
NSString *const kEmailBody = @"";
NSString *const kEmailRecipent = @"info@radiofilotimo.com";

// MobiAds
NSString *const kMobiAddUnitId = @"ca-app-pub-6942298485438468/9855519637";

// Tweet
NSString *const kTweetText = @"";

// Facebook
NSString *const kFacebookUrl = @"https://www.facebook.com/mkistok/";
NSString *const kFacebookDescription = @"";

@end
