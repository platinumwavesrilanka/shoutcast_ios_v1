//
//  Constant.m
//  SoundCast
//
//  Created by Platinum Lanka Pvt Ltd on 1/2/17.
//  Copyright © 2017 Platinum Lanka Pvt Ltd. All rights reserved.
//

#import "Constant.h"

@implementation Constant

// Title
NSString *const kTitle = @"Impacto2 - Cuenca";

// Streaming constants
NSString *const kStreamIpAddress = @"http://69.46.24.226:7139/live";

// Calling and SMS
NSString *const kPhoneNumber = @"7182859053";
NSString *const kSMSBody = @"";

// Email
NSString *const kEmailTitle = @"Title";
NSString *const kEmailBody = @"";
NSString *const kEmailRecipent = @"radioimpacto2@hotmail.com";

// MobiAds
NSString *const kMobiAddUnitId = @"ca-app-pub-6942298485438468/8659076436";

// Tweet
NSString *const kTweetText = @"";

// Facebook
NSString *const kFacebookUrl = @"https://www.facebook.com/mkistok/";
NSString *const kFacebookDescription = @"";

@end
