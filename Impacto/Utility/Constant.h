//
//  Constant.h
//  SoundCast
//
//  Created by Platinum Lanka Pvt Ltd on 1/2/17.
//  Copyright © 2017 Platinum Lanka Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constant : NSObject

// Title
FOUNDATION_EXPORT NSString *const kTitle;

// Streaming
FOUNDATION_EXPORT NSString *const kStreamIpAddress;

// Calling and SMS
FOUNDATION_EXPORT NSString *const kPhoneNumber;
FOUNDATION_EXPORT NSString *const kSMSBody;

// Email
FOUNDATION_EXPORT NSString *const kEmailTitle;
FOUNDATION_EXPORT NSString *const kEmailBody;
FOUNDATION_EXPORT NSString *const kEmailRecipent;

// MobiAds
FOUNDATION_EXPORT NSString *const kMobiAddUnitId;

// Tweet
FOUNDATION_EXPORT NSString *const kTweetText;

// Facebook
FOUNDATION_EXPORT NSString *const kFacebookUrl;
FOUNDATION_EXPORT NSString *const kFacebookDescription;

@end
