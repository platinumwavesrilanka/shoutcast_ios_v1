//
//  PlayViewController+NWPlayViewController.m
//  ShoutCast
//
//  Created by Platinum Lanka Pvt Ltd on 2/14/17.
//  Copyright © 2017 Platinum Lanka Pvt Ltd. All rights reserved.
//

#import "PlayViewController+NWPlayViewController.h"

@implementation PlayViewController (NWPlayViewController)


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupPlayer];
    
    // hide top Button
    [self.messageButton setHidden:YES];
    [self.callButton setHidden:YES];
    [self.mailButton setHidden:YES];
}

-(void)viewDidAppear:(BOOL)animated {
    [self becomeFirstResponder];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    
}

@end
