//
//  PlayingInfoNofificationCenter.m
//  AudioPlayer
//
//  Created by Platinum Lanka Pvt Ltd on 12/27/16.
//  Copyright © 2016 Platinum Lanka Pvt Ltd. All rights reserved.
//

#import "PlayingInfoNofificationCenter.h"

@implementation PlayingInfoNofificationCenter

static PlayingInfoNofificationCenter *_sharedInstance = nil;
static dispatch_once_t onceToken;

+ (instancetype) sharedInstance {
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[PlayingInfoNofificationCenter alloc] init];
    });
    
    return _sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.playingInfoCenter = [MPNowPlayingInfoCenter defaultCenter];
    }
    return self;
}

- (void) setPlayingTitle:(NSString *)title artistName:(NSString *)artist {
    MPMediaItemArtwork *artwork = [[MPMediaItemArtwork alloc] initWithImage:[self artWorkImage]];
    self.playingInfoCenter.nowPlayingInfo = @{MPMediaItemPropertyTitle: title, MPMediaItemPropertyArtist: artist, MPMediaItemPropertyArtwork:artwork};
}

- (UIImage *)artWorkImage {
    UIImage *coverImage = [UIImage imageNamed:@"logo"];
    UIGraphicsBeginImageContext(coverImage.size);
    [coverImage drawInRect:CGRectMake(0, 0, coverImage.size.width, coverImage.size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void) play {
   
}

- (void) pause {
    
}
@end
