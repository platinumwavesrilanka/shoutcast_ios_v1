//
//  ViewController.m
//  AudioPlayer
//
//  Created by Platinum Lanka Pvt Ltd on 12/20/16.
//  Copyright © 2016 Platinum Lanka Pvt Ltd. All rights reserved.
//

#import "PlayViewController.h"
#import "PlayingInfoNofificationCenter.h"
#import <TwitterKit/TwitterKit.h>
#import "CustomUISlider.h"
#import "Utility/Utiltiy.h"
#import "Constant.h"
#import <AFNetworking.h>


@import GoogleMobileAds;

NSString *const IP = @"http://72.46.136.154:7225/stream";
NSString *const PHONE_NO = @"0713479874";


@interface PlayViewController ()

@property (atomic, assign) BOOL isPlaying;
@property (weak, nonatomic) IBOutlet UIButton *playerButton;
@property (weak, nonatomic) IBOutlet CustomUISlider *slider;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundView;


@end

@implementation PlayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupPlayer];
}

- (void)viewWillAppear:(BOOL)animated {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self setupViewForIpad];
    }
}

-(void)viewDidAppear:(BOOL)animated {
    [self becomeFirstResponder];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    
    [self requestBannerView];
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupPlayer {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    __block AudioPlayerManager *manager;
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if(status != AFNetworkReachabilityStatusNotReachable) {
                manager = [AudioPlayerManager sharedInstance];
                [manager setupPlayerWith:kStreamIpAddress];
                manager.delegate = self;
        } else {
            [self warningAlert];
        }
    }];
    
    self.isPlaying = NO;
    
    self.titleLabel.text = kTitle;
    
    // Setup blurred Background view
    self.backgroundView.image = [Utiltiy blurredImageWithImage:[UIImage imageNamed:@"platinum_logo"]];
}

// Rcievie events changing from playing info notification center
// In background mode
- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    UIEventSubtype rc = event.subtype;
    NSLog(@"got a remote event! %ld", (long)rc);
    
    if (rc == UIEventSubtypeRemoteControlTogglePlayPause) {
        if ([self isPlaying])
            [[AudioPlayerManager sharedInstance] pause];
        else
            [[AudioPlayerManager sharedInstance] play];
    } else if (rc == UIEventSubtypeRemoteControlPlay) {
         [[AudioPlayerManager sharedInstance] play];
    } else if (rc == UIEventSubtypeRemoteControlPause) {
        [[AudioPlayerManager sharedInstance] pause];
    }
}

-(BOOL) isReachable {
    return [[AFNetworkReachabilityManager sharedManager] isReachable];
}

- (void) warningAlert {
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Warning"
                                 message:@"Internet connection interrupted."
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                }];
    
    [alert addAction:yesButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)setupViewForIpad {
    // Setup Title label for iPad
    CGRect frame = self.titleLabel.frame;
    frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height * 10);
    [self.titleLabel setFrame:frame];
}

// Toggle play buttonx
- (IBAction)playButton:(UIButton *)button {
    if ([self isPlaying]) {
        [[AudioPlayerManager sharedInstance] pause];
        self.isPlaying = NO;
        [self.playerButton setBackgroundImage:[UIImage imageNamed:@"btn_play_normal"] forState:UIControlStateNormal];
        [self.playerButton setBackgroundImage:[UIImage imageNamed:@"btn_play_pressed"] forState:UIControlStateSelected];
        [self.playerButton setBackgroundImage:[UIImage imageNamed:@"btn_play_pressed"] forState:UIControlStateFocused];
        
    } else {
        [[AudioPlayerManager sharedInstance] play];
        self.isPlaying = YES;
        [self.playerButton setBackgroundImage:[UIImage imageNamed:@"btn_puse_normal"] forState:UIControlStateNormal];
        [self.playerButton setBackgroundImage:[UIImage imageNamed:@"btn_puse_pressed"] forState:UIControlStateSelected];
         [self.playerButton setBackgroundImage:[UIImage imageNamed:@"btn_puse_pressed"] forState:UIControlStateFocused];
    }
//    else if(![self isReachable]) {
//        [self warningAlert];
//    }
}

// Observation for required keypath | [status]
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    if((object == self.radioPlayer) && [keyPath isEqualToString:@"status"]) {
        // Player status
        if (self.radioPlayer.status == AVPlayerStatusFailed) {
            NSLog(@"AVPlayer Failed");

        } else if (self.radioPlayer.status == AVPlayerStatusReadyToPlay) {
            NSLog(@"AVPlayerStatusReadyToPlay");
            [self.radioPlayer play];
 
        } else if (self.radioPlayer.status == AVPlayerItemStatusUnknown) {
            NSLog(@"AVPlayer Unknown");
            
        }
    }
}

// UISlider value change event
- (IBAction)sliderValueChanged:(id)sender {
    [[AudioPlayerManager sharedInstance] changeVolume:self.slider.value];
}

// Min Volum Button
- (IBAction)minVolumeButtonPressed:(id)sender {
    int volume = 0.5;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        volume = 0;
    }
    
    [self.slider setValue:volume];
    [[AudioPlayerManager sharedInstance] changeVolume:0.0f];
}

// Max volume button
- (IBAction)maxVolumeButtonPressed:(id)sender {
    [self.slider setValue:3.5f];
    [[AudioPlayerManager sharedInstance] changeVolume:3.5f];
}

// Delegate of player manager
// Update information of the song when it change.
- (void)metaDataForCurrentSong:(Song *)song {
    self.songTitle.text = song.title;
    self.artist.text = song.artist;
    [[PlayingInfoNofificationCenter sharedInstance] setPlayingTitle:song.title artistName:song.artist];
}

// Requesst Banner view
- (void)requestBannerView {
    self.bannerView.adUnitID = kMobiAddUnitId;
    self.bannerView.rootViewController = self;
    
    [self.bannerView loadRequest:[GADRequest request]];
}

#pragma mark - Social media
// Facebook sharing
- (IBAction)facebookShare:(id)sender {
    
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", kFacebookUrl]];
    content.contentDescription = kFacebookDescription;
    
    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fbauth2://"]]){
        dialog.mode = FBSDKShareDialogModeNative;
    }
    else {
        dialog.mode = FBSDKShareDialogModeBrowser;
    }
    
    dialog.shareContent = content;
    dialog.delegate = self;
    dialog.fromViewController = self;
    [dialog show];
}

// Facebook delegate
- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results {
    
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error {
    
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer {
    
}

// Tweet development
- (IBAction)tweet:(id)sender {
    TWTRComposer *composer = [[TWTRComposer alloc] init];
    
    [composer setText:kTweetText];
    [composer setImage:[UIImage imageNamed:@"fabric"]];
    
    // Called from a UIViewController
    [composer showFromViewController:self completion:^(TWTRComposerResult result) {
        if (result == TWTRComposerResultCancelled) {
            NSLog(@"Tweet composition cancelled");
        } else {
            NSLog(@"Sending Tweet!");
        }
    }];
}

// Call programatically
- (IBAction)makeCall:(id)sender {
    
    NSString *cleanedString = [[kPhoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    NSString *escapedPhoneNumber = [cleanedString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *phoneURLString = [NSString stringWithFormat:@"telprompt:%@", escapedPhoneNumber];
    NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneURL]) {
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
}

// Send SMS programatically
- (IBAction)sendSMS:(id)sender {
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText]) {
        controller.body = kSMSBody;
        controller.recipients = [NSArray arrayWithObjects:kPhoneNumber, nil];
        controller.messageComposeDelegate = self;
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:controller animated:YES completion:nil];
    }
}


// Send mail programatically
- (IBAction)sendMail:(id)sender {
    
    if(![MFMailComposeViewController canSendMail]) {
        NSLog(@"Error");
        return;
    }
    
    NSString *emailTitle = kEmailTitle;
    // Email Content
    NSString *messageBody = kEmailBody;
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:kEmailRecipent];
    
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil) {
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
       // [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
}

#pragma mark - Delegate for messages and mails

// Message composer delegate
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    switch (result) {
        case MessageComposeResultCancelled:
            NSLog(@"Cancelled");
            break;
        case MessageComposeResultFailed: {
            NSLog(@"faild");
            UIAlertController *alrt = [UIAlertController alertControllerWithTitle:@"my apps" message:@"unknown error" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                //do something when click button
            }];
            [alrt addAction:okAction];
            break;
        }
        case MessageComposeResultSent:
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Mail composer delegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
