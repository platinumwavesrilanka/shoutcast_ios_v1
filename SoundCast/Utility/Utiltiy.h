//
//  Utiltiy.h
//  SoundCast
//
//  Created by Platinum Lanka Pvt Ltd on 1/10/17.
//  Copyright © 2017 Platinum Lanka Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Utiltiy : NSObject

+ (UIImage *)blurredImageWithImage:(UIImage *)sourceImage;

@end
