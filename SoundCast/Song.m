//
//  Song.m
//  AudioPlayer
//
//  Created by Platinum Lanka Pvt Ltd on 12/20/16.
//  Copyright © 2016 Platinum Lanka Pvt Ltd. All rights reserved.
//

#import "Song.h"

@implementation Song

+ (instancetype)initWithData:(id)data
{
    Song *object = [[Song alloc] init];
    
    object.title = [data objectForKey:@"trackName"];
    object.artist = [data objectForKey:@"artistName"];
    object.album = [data objectForKey:@"collectionName"];
    object.source = [data objectForKey:@"previewUrl"];
    object.artworkURL = [data objectForKey:@"artworkUrl100"];
    
    return object.source ? object : nil;
}

+ (instancetype)initWithID3Tags:(AVMetadataItem *)metadata {
    Song *object = [[Song alloc] init];
    NSString *value = metadata.stringValue;
    
    NSArray *items = [value componentsSeparatedByString:@"-"];
    
    NSString *title  = @"";
    NSString *artistName = @"";
    
    if([items count] >= 1) {
        title = [[items objectAtIndex:0] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    }
    
    if([items count] == 2) {
        artistName = [items objectAtIndex:1];
    }
    
    NSUInteger location = [artistName rangeOfString:@"["].location;
    if(artistName.length > location) {
        artistName = [artistName stringByReplacingCharactersInRange:NSMakeRange(location, (artistName.length - location)) withString:@""];
    }
    
    object.title = title;
    object.artist = artistName;
    
    return object;
}

@end
