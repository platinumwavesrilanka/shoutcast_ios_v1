//
//  Song.h
//  AudioPlayer
//
//  Created by Platinum Lanka Pvt Ltd on 12/20/16.
//  Copyright © 2016 Platinum Lanka Pvt Ltd. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <Foundation/Foundation.h>

@interface Song : NSObject

@property (nonatomic, strong) NSString *source;
@property (nonatomic, strong) NSString *artworkURL;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *artist;
@property (nonatomic, strong) NSString *album;

+ (instancetype)initWithData:(id)data;
+ (instancetype)initWithID3Tags:(AVMetadataItem *)metadata;

@end
