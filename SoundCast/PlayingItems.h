//
//  PlayerItems.h
//  AudioPlayer
//
//  Created by Platinum Lanka Pvt Ltd on 12/20/16.
//  Copyright © 2016 Platinum Lanka Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Song.h"


@interface PlayingItems : NSObject

+ (instancetype)sharedInstance;


@property (nonatomic, strong) NSMutableArray *queueItems;

@end
