//
//  CustomUISlider.m
//  SoundCast
//
//  Created by Platinum Lanka Pvt Ltd on 1/9/17.
//  Copyright © 2017 Platinum Lanka Pvt Ltd. All rights reserved.
//

#import "CustomUISlider.h"

@implementation CustomUISlider


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    UIImage *minImage = [[UIImage imageNamed:@"volume_bar_blue_middle.png"]
                         resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    UIImage *maxImage = [[UIImage imageNamed:@"volume_bar_gray_middle.png"]
                         resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    UIImage *thumbImageNormal = [UIImage imageNamed:@"volume_adjust_normal.png"];
    UIImage *thumbImagePressed = [UIImage imageNamed:@"volume_adjust_pressed.png"];
    
    [self setMaximumTrackImage:maxImage forState:UIControlStateNormal];
    [self setMinimumTrackImage:minImage forState:UIControlStateNormal];

    [self setThumbImage:thumbImageNormal forState:UIControlStateNormal];
    [self setThumbImage:thumbImagePressed forState:UIControlStateHighlighted];
    
}


@end
