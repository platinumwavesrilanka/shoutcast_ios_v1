 //
//  AudioPlayerManager.m
//  AudioPlayer
//
//  Created by Platinum Lanka Pvt Ltd on 12/20/16.
//  Copyright © 2016 Platinum Lanka Pvt Ltd. All rights reserved.
//


#import "AudioPlayerManager.h"
#import "PlayingItems.h"
#import "Song.h"
#import "PlayingInfoNofificationCenter.h"
#import <AFNetworking.h>

@import MediaPlayer;
@import AVFoundation;

@interface AudioPlayerManager () {
    BOOL isNetworkInterupted;
    NSString *ipAddress;
}

@property (atomic, assign) AVPlayerStatus playerStatus;
@property (nonatomic, retain) PlayingInfoNofificationCenter *playingNotificationCenter;

@end

@implementation AudioPlayerManager

static AudioPlayerManager *_sharedInstance = nil;
static dispatch_once_t onceToken;

+ (instancetype)sharedInstance
{
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        isNetworkInterupted = NO;
    }
    return self;
}

/*
 * Setup audio player
 */
- (void)setupPlayerWith:(NSString *)ip {
    
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback
                                           error:&sessionError];
    [[AVAudioSession sharedInstance] setActive: YES error: NULL];
    
    AVPlayerItem *playerItem = [[AVPlayerItem alloc] initWithURL:[NSURL URLWithString:ip]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerItemFailedToPlayToEndTime:) name:AVPlayerItemFailedToPlayToEndTimeNotification object:playerItem];
    
    
    
    [playerItem addObserver:self forKeyPath:@"timedMetadata" options:NSKeyValueObservingOptionNew context:NULL];
    
    AVPlayer *player = [[AVPlayer alloc] initWithPlayerItem: playerItem];
    self.radioPlayer = player;
    
    [playerItem addObserver:self forKeyPath:@"playbackBufferEmpty" options:NSKeyValueObservingOptionNew context:nil];
    
    [self.radioPlayer addObserver:self forKeyPath:@"status" options:0 context:nil];
    
    self.playingNotificationCenter = [PlayingInfoNofificationCenter sharedInstance];
    NSMutableDictionary *songInfo = [NSMutableDictionary dictionaryWithDictionary:[MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo];
    [songInfo setObject:[NSNumber numberWithFloat:1.0f] forKey:MPNowPlayingInfoPropertyPlaybackRate];
    self.playingNotificationCenter.playingInfoCenter.nowPlayingInfo = songInfo;
}



#pragma mark - Observer and Notification events
/*
 * Add Observer into radio player
 */
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    if([keyPath isEqualToString:@"playbackBufferEmpty"]) {
        if (self.radioPlayer.currentItem.playbackBufferEmpty) {
            //Your code here
        }
    }
    
    // AVPlayer status observer
    if((object == self.radioPlayer) && [keyPath isEqualToString:@"status"]) {
        // Player status
        self.playerStatus = self.radioPlayer.status;
        
        if (self.radioPlayer.status == AVPlayerStatusFailed) {
            NSLog(@"AVPlayer Failed");
            
        } else if (self.radioPlayer.status == AVPlayerStatusReadyToPlay) {
            NSLog(@"AVPlayerStatusReadyToPlay");
            
        } else if (self.radioPlayer.status == AVPlayerItemStatusUnknown) {
            NSLog(@"AVPlayer Unknown");
            
        }
    } else if([object class] == [AVPlayerItem class]) {
        AVPlayerItem *playerItem = (AVPlayerItem *)object;
        NSArray *metadataList = playerItem.timedMetadata;
        
        for (AVMetadataItem *metadata in metadataList) {
            NSLog(@"\nkey: %@\nkeySpace: %@\ncommonKey: %@\nvalue: %@", [metadata.key description], metadata.keySpace, metadata.commonKey, metadata.stringValue);
            Song *song = [Song initWithID3Tags:metadata];
            if([self.delegate respondsToSelector:@selector(metaDataForCurrentSong:)]) {
                [self.delegate metaDataForCurrentSong:song];
            }
        }
    }
}

- (void) playerItemFailedToPlayToEndTime:(NSNotification *)notification
{
    NSError *error = notification.userInfo[AVPlayerItemFailedToPlayToEndTimeErrorKey];
    [self.radioPlayer pause];
    isNetworkInterupted = YES;
}

- (void) playerItemJumpToPlay:(NSNotification *)notification
{
    NSError *error = notification.userInfo[AVPlayerItemFailedToPlayToEndTimeErrorKey];
    [self.radioPlayer pause];
}

#pragma mark - Methods
- (void) play {
    if(self.playerStatus == AVPlayerStatusReadyToPlay) {
        [self.radioPlayer play];
    }
}


- (void) pause {
    [self.radioPlayer pause];
}

- (void) changeVolume:(CGFloat)volume {
    [self.radioPlayer setVolume:volume];

}

@end
