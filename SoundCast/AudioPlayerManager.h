//
//  AudioPlayerManager.h
//  AudioPlayer
//
//  Created by Platinum Lanka Pvt Ltd on 12/20/16.
//  Copyright © 2016 Platinum Lanka Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "Song.h"

@protocol AudioPlayerManagerDelegate

@required
-(void)metaDataForCurrentSong:(Song *)song;

@end

@interface AudioPlayerManager : NSObject {
   __weak id <AudioPlayerManagerDelegate> _delegate;

}

@property (nonatomic, retain) AVPlayer *radioPlayer;
@property (weak, nonatomic) id  delegate;

+(instancetype)sharedInstance;
- (void)setupPlayerWith:(NSString *)ip;
- (void) play;
- (void) pause;
- (void) changeVolume:(CGFloat)volume;

@end
