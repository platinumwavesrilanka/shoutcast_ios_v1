//
//  PlayingInfoNofificationCenter.h
//  AudioPlayer
//
//  Created by Platinum Lanka Pvt Ltd on 12/27/16.
//  Copyright © 2016 Platinum Lanka Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>

@interface PlayingInfoNofificationCenter : NSObject

@property (nonatomic, retain) MPNowPlayingInfoCenter *playingInfoCenter;

+ (instancetype) sharedInstance;
- (void) setPlayingTitle:(NSString *)title artistName:(NSString *)artist;

@end
