//
//  ViewController.h
//  AudioPlayer
//
//  Created by Platinum Lanka Pvt Ltd on 12/20/16.
//  Copyright © 2016 Platinum Lanka Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFHTTPSessionManager.h>
#import "AudioPlayerManager.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@class GADBannerView;

@interface PlayViewController : UIViewController<AudioPlayerManagerDelegate, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate, FBSDKSharingDelegate>

@property (nonatomic, retain) AVPlayer *radioPlayer;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintForSlider;


@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *songTitle;

@property (weak, nonatomic) IBOutlet UILabel *artist;

@property (weak, nonatomic) IBOutlet UIImageView *bottomViewImage;

@property (weak, nonatomic) IBOutlet UIImageView *borderViewImage;

@property (weak, nonatomic) IBOutlet UIButton *callButton;

@property (weak, nonatomic) IBOutlet UIButton *messageButton;

@property (weak, nonatomic) IBOutlet UIButton *mailButton;
@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;

- (void)setupPlayer;

- (void)metaDataForCurrentSong:(Song *)song;


@end

