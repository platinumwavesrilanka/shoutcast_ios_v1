//
//  Constant.m
//  SoundCast
//
//  Created by Platinum Lanka Pvt Ltd on 1/2/17.
//  Copyright © 2017 Platinum Lanka Pvt Ltd. All rights reserved.
//

#import "Constant.h"

@implementation Constant

// Title
NSString *const kTitle = @"MK IStock Radio";

// Streaming constants
NSString *const kStreamIpAddress = @"http://213.239.218.99:7730";

// Calling and SMS
NSString *const kPhoneNumber = @"0713479874";
NSString *const kSMSBody = @"";

// Email
NSString *const kEmailTitle = @"Title";
NSString *const kEmailBody = @"";
NSString *const kEmailRecipent = @"wave@comcities.com";

// MobiAds
NSString *const kMobiAddUnitId = @"ca-app-pub-6942298485438468/8659076436";

// Tweet
NSString *const kTweetText = @"";

// Facebook
NSString *const kFacebookUrl = @"https://www.facebook.com/mkistok/";
NSString *const kFacebookDescription = @"";

@end
