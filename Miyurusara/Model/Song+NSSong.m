//
//  Song+NSSong.m
//  ShoutCast
//
//  Created by Platinum Lanka Pvt Ltd on 2/3/17.
//  Copyright © 2017 Platinum Lanka Pvt Ltd. All rights reserved.
//

#import "Song+NSSong.h"

@implementation Song (NSSong)

+ (instancetype)initWithID3Tags:(AVMetadataItem *)metadata {
    Song *object = [[Song alloc] init];
    NSString *value = metadata.stringValue;
    
    NSArray *items = [value componentsSeparatedByString:@"-"];
    
    NSString *title  = @"";
    NSString *artistName = @"";
    
    if([items count] >= 1) {
        artistName = [[items objectAtIndex:0] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    }
    
    if([items count] == 2) {
        title = [items objectAtIndex:1];
    }
    
    NSUInteger location = [artistName rangeOfString:@"["].location;
    if(artistName.length > location) {
        artistName = [artistName stringByReplacingCharactersInRange:NSMakeRange(location, (artistName.length - location)) withString:@""];
    }
    
    object.title = title;
    object.artist = artistName;
    
    return object;
}


@end
