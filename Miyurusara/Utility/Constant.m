//
//  Constant.m
//  SoundCast
//
//  Created by Platinum Lanka Pvt Ltd on 1/2/17.
//  Copyright © 2017 Platinum Lanka Pvt Ltd. All rights reserved.
//

#import "Constant.h"

@implementation Constant

// Title
NSString *const kTitle = @"Miyurusara";

// Streaming constants
NSString *const kStreamIpAddress = @"http://76.164.217.100:7282/stream";

// Calling and SMS
NSString *const kPhoneNumber = @"0713479874";
NSString *const kSMSBody = @"";

// Email
NSString *const kEmailTitle = @"Title";
NSString *const kEmailBody = @"";
NSString *const kEmailRecipent = @"wave@comcities.com";

// MobiAds
NSString *const kMobiAddUnitId = @"ca-app-pub-6942298485438468/8659076436";

// Tweet
NSString *const kTweetText = @"";

// Facebook
NSString *const kFacebookUrl = @"https://www.facebook.com/miyurusara/";
NSString *const kFacebookDescription = @"";

@end
