//
//  PlayViewController+NSPlayViewController.m
//  ShoutCast
//
//  Created by Platinum Lanka Pvt Ltd on 2/2/17.
//  Copyright © 2017 Platinum Lanka Pvt Ltd. All rights reserved.
//

#import "PlayViewController+NSPlayViewController.h"
#import "Constant.h"

@implementation PlayViewController (NSPlayViewController)

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupPlayer];
    
    // hide top Button
    [self.messageButton setHidden:YES];
    [self.callButton setHidden:YES];
    [self.mailButton setHidden:YES];
    
    
    // Change UILabel colors
    [self.titleLabel setTextColor:[UIColor blackColor]];
    [self.artist setTextColor:[UIColor blackColor]];
    [self.songTitle setTextColor:[UIColor blackColor]];
    
    // Change text of title
    [self.titleLabel setText:kTitle];
    
    [self.bottomViewImage setAlpha:0.1];
}

@end
